# SISTER LOBA / Digital Bicycle Mapping Prototype, Analogue Toolkit and Challenge.
### Second Iteration
### Fab Academy Challenge 4

The following project was co-authored with MDEF colleague Verónica Agreda de Pazos as part of the Fab Academy program in the Fab Lab Barcelona and co-design and inspired by the cyclists women around the world.
<br>


![SISTERLOBA_GPS_FAIRMOBILITY](PHOTOS/SISTERLOBA_GPS_FAIRMOBILITY.jpg)

<br>

## PROBLEMS WE ARE FACING

- Climate Emergency

-  Car industry and CO2 emissions

-  Unfair Mobility and inclusion issues

-	#WomenSafety is global issue. Globally, almost one in three women (an estimated 736 million) have experienced physical and/or sexual violence. Fewer than 40% of the women who experience violence seek help of any sort. Very few look to formal institutions, such as police (fewer than 10%) and health services (UN Women, 2021).

Globally, an estimated 736 million women, almost one in three have experienced physical and/or sexual violence and the transportation systems in cities are not violence free.

Less than 40% of the women who experience violence seek help of any sort. Among women who do seek help, most look to family and friends, and very few look to formal institutions, such as police (less than 10%) and health services. (UN Women, 2021).

Public transportation systems are not always well connected. Cycling is an affordable alternative to promote fair mobility, security and wellbeing. Listening to women sharing their cycling experience in the cities and involving them in the policy making process is imperative if we want to build fair and sustainable policies.

Collective data provides unprecedented opportunities for understanding and planning urban mobility from a (bottom-up) co-designed approach.

<br>

![PCB_PURPLE_FAIRMOBILITY](PHOTOS/PCB_PURPLE_FAIRMOBILITY.jpg)

<br>

## A NEED FOR HUMANIZING DATA
<br>

Big data is not the ultimate solution for dealing with urban mobility, especially when considering **social dimensions.**
Google's Map algorithms will find the shortest path and alternative routes in real time. But, **are the shortest paths the safest ones for cyclists?**

How can we build human centered big data based on the women’s cyclist experiences and needs?

How could this ‘humanized’ big data contribute to mobility planning and policy approaches that may effectively promote cycling and enable women's security and wellbeing?  

How can we build a women’s cyclists sisterhood establishing sorority agreements?

Since our fight is fair mobility and bridging the gender gap, we decided to create synergies and to tackle this problem together.

![SISTERLOBA_LOGO](PHOTOS/SISTERLOBA_LOGO.png)


## OUR RESPONCE
<br>

**Our purpose is to empower women cyclists.**
After a co-creation process with women cyclists of our communities in Barcelona (Spain), Santa Cruz de la Sierra (Bolivia) and León (México) we designed a digital mapping prototype and an analogue toolkit to allow them to map and rate their cyclist experience in their city in terms of safety and quality. Theirs insights are valuable while developing fair mobility practices and designing bottom-up policies.


## ANALOGUE TOOLKIT
<br>

![ANALOGUE_FAIRMOBILITY](PHOTOS/ANALOGUE_FAIRMOBILITY.jpg)

 City map with the different spots of the challenge, a kit of 6 crayons and QR stickers for the questions. With the analog toolkit, cyclists can grade and rank the bicycle paths and the alternative routes during the challenge.

<br>

## DIGITAL BICYCLE MAPPING PROTOTYPE 

<br>

![SISTERLOBA_WHITE_FAIRMOBILITY](PHOTOS/SISTERLOBA_WHITE_FAIRMOBILITY.jpg)


The Digital Bike Mapping Prototype is a DIY microcomputer attached to the bicycle that tracks your route and enables you to map them according to a color code, based on  your personal experience. The data is stored and you can choose to share it, download it and use it for the collective benefit.

## GPS NEO-6M MODULE
<br>

![GPS_SISTER_LOBA_WEB_](PHOTOS/GPS_SISTER_LOBA_WEB_.jpg)

<br>

### TECHNICAL SPECIFICATIONS
<br>

- Supply Voltage: 3.3 ~ 5 V
- Current consumption: 45 mA
- Communication: UART
- Communication speed: 9600 bps
- Memory: EEPROM
- Backup battery
- GPS lock indicator LED
- Default baud rate: 9600
- It is compatible with Arduino, PIC, AVR, Raspberry and other microcontrollers on the market and can deliver accurate information as well as be established through the UART port

<br>

### FEATURES
<br>

- Antenna size: 25 x 25 mm
- Module size: 25 x 35 mm
- Weight: 16 g

<br>

### CODING LOGIC C++
<br>


```c

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// Aactivating the devise

bool buttonPressed = false;
bool tracking = false;

static const int RXPin = 33, TXPin = 32;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

#include <WiFi.h>
#include <Wire.h>

// Connecting to Wifi

#define WIFI_SSID "add network"
#define  WIFI_PASSWORD "your password"
const char* resource = "/trigger/BIKE_GPS_READINGS/with/key/euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz";
const char* server = "maker.ifttt.com";

unsigned long previousMillis = 0;        // will store last time GPS was updated


const long interval = 5000;             // constants won't change:

void setup() {
  ss.begin(GPSBaud);

  pinMode(21, INPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(13, OUTPUT);

  Serial.begin(115200);

  Serial.print("Connecting to ");
  WiFi.mode(WIFI_STA);   //   create wifi station
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // GPS Characteristics 
  while (ss.available() > 0) {
    gps.encode(ss.read());
    if (gps.location.isUpdated()) {
      // Latitude in degrees (double)
      int sat = gps.satellites.value();
      Serial.println(sat);
      if (sat >= 3) {
        digitalWrite(13, HIGH);
        Serial.println("ON");
      }
      else {
        digitalWrite(13, LOW);
        Serial.println("OFF");
      }
      
//      // Horizontal Dim. of Precision (100ths-i32)
//      Serial.print("HDOP = ");
//      Serial.println(gps.hdop.value());
    }
  }

  if (digitalRead(21) == LOW && buttonPressed == false) {
    if (tracking == false) {
      digitalWrite(16, HIGH); // Code Neopixel Encender
      digitalWrite(17, HIGH);
      tracking = true;
    }
    else {
      digitalWrite(16, LOW); // Code Neopixel Apagar
      digitalWrite(17, LOW);
      tracking = false;
    }
    //tracking = true;
    buttonPressed = true;

  }

  if (digitalRead(21) == HIGH) {
    buttonPressed = false;
  }
  delay(10);

  if (tracking) {
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;

      makeIFTTTRequest();
      Serial.println("Data sent");
      //delay(1000);

    }
  }
}

void makeIFTTTRequest() {
  Serial.print("Connecting to ");
  Serial.print(server);

  WiFiClient client;
  int retries = 5;
  while (!!!client.connect(server, 80) && (retries-- > 0)) {
    Serial.print(".");
  }
  Serial.println();
  if (!!!client.connected()) {
    Serial.println("Failed to connect...");
  }

  Serial.print("Request resource: ");
  Serial.println(resource);

  Serial.println(gps.location.lat(), 6);

  // Value in datasheet
  String jsonObject = String("{\"value1\":\"") + String(gps.location.lat(), 6) + "\",\"value2\":\"" + String(gps.location.lng(), 6)
                      + "\",\"value3\":\"" + gps.speed.kmph() + "\"}";

  Serial.println(jsonObject);

  client.println(String("POST ") + resource + " HTTP/1.1");
  client.println(String("Host: ") + server);
  client.println("Connection: close\r\nContent-Type: application/json");
  client.print("Content-Length: ");
  client.println(jsonObject.length());
  client.println();
  client.println(jsonObject);

  int timeout = 5 * 10; // 5 seconds
  while (!!!client.available() && (timeout-- > 0)) {
    delay(100);
  }
  if (!!!client.available()) {
    Serial.println("No response...");
  }
  while (client.available()) {
    Serial.write(client.read());
  }

  Serial.println("\nclosing connection");
  client.stop();
}

 ```

![GPS_Readings_SISTER_LOBA](PHOTOS/GPS_Readings_SISTER_LOBA.png)

## PCB

To fabricate the PCB first we tested the Arduino Code with the Breadboard and the ESP32 Adafruit Feather with a Neo-6m GPS Module.  Then we made another version that include one more LED that indicates the minimum  number of satellites required for an accurate geo reference. 

![PCB_COMPONENTS_FAIR_MOBILITY_WEB_](PHOTOS/PCB_COMPONENTS_FAIR_MOBILITY_WEB_.jpg)

We used the KiCad program for the circuits design and then for the vectors editing we used InkScape (open source) and also Adobe Illustrator (commercial). It is necessary to export 3 different files that can be perfectly alined in 1000 dpi. The first file is for the engraving of the Traces, second one for the Holes where the ESP32 fits, and finally the Outline for cutting the board. For the PCB fabrication we used a CNC mini milling SMR-20 monoFAB Roland machine. The milling process needs to be engraved and cutted in that order too: traces, holes and outline. 


![SNAPPING_PCB_FAIR_MOBILITY](PHOTOS/SNAPPING_PCB_FAIR_MOBILITY.mp4)

To power the ESP32, the GPS module , and the LEDS we used a 5 V lithium battery.

## 3D PRINTING
<br>

![PCB_FAIRMOBILITY](PHOTOS/PCB_FAIRMOBILITY.jpg)

To protect the circuits and electronics was necessary to integrate all the parts  inside a press-fit 3D printed case that consist in 3 parts: the lit with the PCB button access and LEDS, the middle chamber with columns to hold the ESP32 and the GPS, and the bottom part with the switcher, the lithium battery and the holes for screwing the bicycle handler ring.
<br>

![BATTERY_FAIRMOBILITY](PHOTOS/BATTERY_FAIRMOBILITY.jpg)

<br>

![3D_CAD_FAIR_MOBILITY](PHOTOS/3D_CAD_FAIR_MOBILITY.mov)

<br>

## MOLDING AND CASTING
<br>

In order to protect the Digital Mapping Prototype from falling on the ground  or from different weather conditions, we designed a silicon wrap with the Sister Loba logo that also works as  button to send the GPS data to the Google Sheet.  The mold was 3d printed and then casted with silicon. 

**Note:** Is important to consider that not all silicon works with 3d printed PLA molds


# LINKS & LIBRARIES
<br>

![MILESTONE_FAIRMOBILITY](PHOTOS/MILESTONE_FAIRMOBILITY.jpg)

[ESP32 (Arduino) Based Bicycle Computer with GPS and SD logging](https://github.com/wilhelmzeuschner/esp32_gps_bicycle_computer/blob/master/code/BicycleComputer_ESP32_2019.ino)

[Footprint Module ESP32 Adafruit Feather](https://kicad.github.io/footprints/Module)

[ESP32 Adatruit Feather Pin Out](https://raw.githubusercontent.com/jango-fx/Adafruit-HUZZAH32-Feather-pinout/master/Adafruit-HUZZAH32-Feather-pinout.png)

[UART](https://circuits4you.com/2018/12/31/esp32-hardware-serial2-example/)

[GPS and Arduino](http://esalvage.blogspot.com/2017/04/gps-and-arduino.html)

[TinyGPSPlus](https://github.com/mikalhart/TinyGPSPlus)

[Arduiniana Tiny GPS ++](http://arduiniana.org/libraries/tinygpsplus/)

[KiCad Library](https://github.com/megasaturnv/kicad-custom-components-library)

[Send GPS location via SMS](https://mechatrofice.com/arduino/send-gps-location-via-sms)

[OPEN FRAMEWORKS C++](https://openframeworks.cc/)


### OPEN SOURCE MAPS
<br>

[Open Street Map](https://www.openstreetmap.org/note/2585591#map=15/41.3844/2.1775&layers=N)

[Map Box](https://www.mapbox.com/dash?gclid=CjwKCAjw7J6EBhBDEiwA5UUM2lG1MxHnttcLbycNj8m8uxHyFcIrC931vnkhxQYCn_f4z-hbvxkleRoCT9IQAvD_BwE)


### TUTORIALS
<br>

[MySQL](https://randomnerdtutorials.com/esp32-esp8266-mysql-database-php/)

[GPS module](https://randomnerdtutorials.com/guide-to-neo-6m-gps-module-with-arduino/)

[Sensor Readings to Google sheet](https://randomnerdtutorials.com/esp32-esp8266-publish-sensor-readings-to-google-sheets/)

[IFTTT](https://maker.ifttt.com/use/euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz)

[APPLETS](https://ifttt.com/applets/zxaTN4iQ/edit)

<br>

# #SISTERLOBACHALLENGE

![SISTERLOBA_TEST_FAIRMOBILITY](PHOTOS/SISTERLOBA_TEST_FAIRMOBILITY.jpg)

### **To test the Analogue Toolkit and the Digital Mapping Prototype we co-designed the #SisterLobaChallenge.**

The women cyclist collectives helped us to choose emblematic places, interesting and leisure places, tourist attractions, historic places, meeting points and iconic places as milestones.

We set up posters with a QR Code in the milestones to let them answer our questions. We invited them to choose their route in order to get out of their comfort zone, to join the challenge and live the fully experience while rating the paths and routes of their city.

<br>

![QR_CODE_SISTER_LOBA](PHOTOS/QR_CODE_SISTER_LOBA.jpg)

<br>

During the challenge or after finishing it, women cyclists can draw and rate the paths based on our color code. They will decide if they want to share or not the georeferenced positions of the bike during the ride after rating the path that they chose. We respect the cyclist's privacy and their right to use their own information and also encourage sharing this data for collective benefit. It is necessary to visualize the problems that cyclists deal with every day in order to create fair policies.

While the cyclists were participating, pedestrians joined the challenge using the analogue toolkit.

<br>

![SISTERLOBA_PHONE_FAIRMOBILITY](PHOTOS/SISTERLOBA_PHONE_FAIRMOBILITY.jpg)

<br>

## KEY MEASUREMENT FACTORS

We have defined six measurement factors to be studied during the challenge, these factors are around problems such as danger, traffic and skilled routes. Also with very positive opportunities that mobility can enhance, for example, connection with nature, inclusiveness in human variations and cultural identity.

<br>

![COLOR_CODE_SISTERLOBA](PHOTOS/COLOR_CODE_SISTERLOBA.jpg)

## SPOTS IN THE CITY
<br>


The spots for the challenge can be decided by the cyclist's own reflections about the topics and they will be shared with geolocation points (latitude and longitude) and visualized with stickers with QR links to the questions.

<br>

![MAPA_SL_CHALLENGE](PHOTOS/MAPA_SL_CHALLENGE.jpg)

<br>

# FUTURE DEVELOPMENT

Thanks to the #SISTERLOBACHALLENGE we were able to test the Digital Mapping Prototype as well as the Analogue Toolkit and the next steps would be to integrate the ESP32 micro controller and the GPS to a single PCB to reduce the size of the Digital Bicycle Mapping and also to develop a mapping platform using all ready existing and open source like the Smart Citizen. With this, we can create a collective mapping of the city to enhance the creation of fair mobility policies based on the participation of cyclists. 

<br>


![MAPA_DIGITAL](PHOTOS/MAPA_DIGITAL.jpg)

![SL_QUESTIONS](PHOTOS/SL_QUESTIONS.jpg)

<br>

We would like to extend our gratitude to the people that helped us during the project and to all who support and participate in  making cities safe and sustainable.

<br>

![LICENSE_FAIR_MOB_](PHOTOS/LICENSE_FAIR_MOB_.png)