#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// Aactivating the devise

bool buttonPressed = false;
bool tracking = false;

static const int RXPin = 33, TXPin = 32;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

#include <WiFi.h>
#include <Wire.h>

// Connecting to Wifi

#define WIFI_SSID "add network"
#define  WIFI_PASSWORD "your password"
const char* resource = "/trigger/BIKE_GPS_READINGS/with/key/euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz";
const char* server = "maker.ifttt.com";

unsigned long previousMillis = 0;        // will store last time GPS was updated


const long interval = 5000;             // constants won't change:

void setup() {
  ss.begin(GPSBaud);

  pinMode(21, INPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(13, OUTPUT);

  Serial.begin(115200);

  Serial.print("Connecting to ");
  WiFi.mode(WIFI_STA);   //   create wifi station
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // GPS Characteristics 
  while (ss.available() > 0) {
    gps.encode(ss.read());
    if (gps.location.isUpdated()) {
      // Latitude in degrees (double)
//      Serial.print("Latitude= ");
//      Serial.print(gps.location.lat(), 6);
//      // Longitude in degrees (double)
//      Serial.print(" Longitude= ");
//      Serial.println(gps.location.lng(), 6);
//
//      // Raw latitude in whole degrees
//      Serial.print("Raw latitude = ");
//      Serial.print(gps.location.rawLat().negative ? "-" : "+");
//      Serial.println(gps.location.rawLat().deg);
//      // ... and billionths (u16/u32)
//      Serial.println(gps.location.rawLat().billionths);
//
//      // Raw longitude in whole degrees
//      Serial.print("Raw longitude = ");
//      Serial.print(gps.location.rawLng().negative ? "-" : "+");
//      Serial.println(gps.location.rawLng().deg);
//      // ... and billionths (u16/u32)
//      Serial.println(gps.location.rawLng().billionths);
//
//      // Raw date in DDMMYY format (u32)
//      Serial.print("Raw date DDMMYY = ");
//      Serial.println(gps.date.value());
//
//      // Year (2000+) (u16)
//      Serial.print("Year = ");
//      Serial.println(gps.date.year());
//      // Month (1-12) (u8)
//      Serial.print("Month = ");
//      Serial.println(gps.date.month());
//      // Day (1-31) (u8)
//      Serial.print("Day = ");
//      Serial.println(gps.date.day());
//
//      // Raw time in HHMMSSCC format (u32)
//      Serial.print("Raw time in HHMMSSCC = ");
//      Serial.println(gps.time.value());
//
//      // Hour (0-23) (u8)
//      Serial.print("Hour = ");
//      Serial.println(gps.time.hour());
//      // Minute (0-59) (u8)
//      Serial.print("Minute = ");
//      Serial.println(gps.time.minute());
//      // Second (0-59) (u8)
//      Serial.print("Second = ");
//      Serial.println(gps.time.second());
//      // 100ths of a second (0-99) (u8)
//      Serial.print("Centisecond = ");
//      Serial.println(gps.time.centisecond());
//
//      // Raw speed in 100ths of a knot (i32)
//      Serial.print("Raw speed in 100ths/knot = ");
//      Serial.println(gps.speed.value());
//      // Speed in knots (double)
//      Serial.print("Speed in knots/h = ");
//      Serial.println(gps.speed.knots());
//      // Speed in miles per hour (double)
//      Serial.print("Speed in miles/h = ");
//      Serial.println(gps.speed.mph());
//      // Speed in meters per second (double)
//      Serial.print("Speed in m/s = ");
//      Serial.println(gps.speed.mps());
//      // Speed in kilometers per hour (double)
//      Serial.print("Speed in km/h = ");
//      Serial.println(gps.speed.kmph());
//
//      // Raw course in 100ths of a degree (i32)
//      Serial.print("Raw course in degrees = ");
//      Serial.println(gps.course.value());
//      // Course in degrees (double)
//      Serial.print("Course in degrees = ");
//      Serial.println(gps.course.deg());
//
//      // Raw altitude in centimeters (i32)
//      Serial.print("Raw altitude in centimeters = ");
//      Serial.println(gps.altitude.value());
//      // Altitude in meters (double)
//      Serial.print("Altitude in meters = ");
//      Serial.println(gps.altitude.meters());
//      // Altitude in miles (double)
//      Serial.print("Altitude in miles = ");
//      Serial.println(gps.altitude.miles());
//      // Altitude in kilometers (double)
//      Serial.print("Altitude in kilometers = ");
//      Serial.println(gps.altitude.kilometers());
//      // Altitude in feet (double)
//      Serial.print("Altitude in feet = ");
//      Serial.println(gps.altitude.feet());
//
//      // Number of satellites in use (u32)
//      Serial.print("Number os satellites in use = ");
//      Serial.println(gps.satellites.value());
      int sat = gps.satellites.value();
      Serial.println(sat);
      if (sat >= 3) {
        digitalWrite(13, HIGH);
        Serial.println("ON");
      }
      else {
        digitalWrite(13, LOW);
        Serial.println("OFF");
      }
      
//      // Horizontal Dim. of Precision (100ths-i32)
//      Serial.print("HDOP = ");
//      Serial.println(gps.hdop.value());
    }
  }

  if (digitalRead(21) == LOW && buttonPressed == false) {
    if (tracking == false) {
      digitalWrite(16, HIGH); // Code Neopixel Encender
      digitalWrite(17, HIGH);
      tracking = true;
    }
    else {
      digitalWrite(16, LOW); // Code Neopixel Apagar
      digitalWrite(17, LOW);
      tracking = false;
    }
    //tracking = true;
    buttonPressed = true;

  }

  if (digitalRead(21) == HIGH) {
    buttonPressed = false;
  }
  delay(10);

  if (tracking) {
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;

      makeIFTTTRequest();
      Serial.println("Data sent");
      //delay(1000);

    }
  }
}

void makeIFTTTRequest() {
  Serial.print("Connecting to ");
  Serial.print(server);

  WiFiClient client;
  int retries = 5;
  while (!!!client.connect(server, 80) && (retries-- > 0)) {
    Serial.print(".");
  }
  Serial.println();
  if (!!!client.connected()) {
    Serial.println("Failed to connect...");
  }

  Serial.print("Request resource: ");
  Serial.println(resource);

  Serial.println(gps.location.lat(), 6);

  // Value in datasheet
  String jsonObject = String("{\"value1\":\"") + String(gps.location.lat(), 6) + "\",\"value2\":\"" + String(gps.location.lng(), 6)
                      + "\",\"value3\":\"" + gps.speed.kmph() + "\"}";

  Serial.println(jsonObject);

  client.println(String("POST ") + resource + " HTTP/1.1");
  client.println(String("Host: ") + server);
  client.println("Connection: close\r\nContent-Type: application/json");
  client.print("Content-Length: ");
  client.println(jsonObject.length());
  client.println();
  client.println(jsonObject);

  int timeout = 5 * 10; // 5 seconds
  while (!!!client.available() && (timeout-- > 0)) {
    delay(100);
  }
  if (!!!client.available()) {
    Serial.println("No response...");
  }
  while (client.available()) {
    Serial.write(client.read());
  }

  Serial.println("\nclosing connection");
  client.stop();
}
